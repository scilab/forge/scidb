<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Copyright (C) Igor GRIDCHYN
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->

<refentry xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook"
          version="5.0-subset Scilab"
          xml:lang="en"
          xml:id="DbFuzzyDelete">

    <refnamediv>
        <refname>DbFuzzyDelete</refname>
        <refpurpose>delete fuzzy object from local FMB</refpurpose>
    </refnamediv>

    <!-- ===================================================================== -->
    <!-- Calling Sequence -->
    <!-- ===================================================================== -->
    <refsynopsisdiv>
        <title>Calling Sequence</title>
        <synopsis>
          DbFuzzyDelete(fuzzySQL, objectType, objectIntId)
          DbFuzzyDelete(fuzzySQL, objectType, objectStringId)
          DbFuzzyDelete(fuzzySQL, objectType, objectIntId, ObjectStringId)
          DbFuzzyDelete(fuzzySQL, objectType, objectIntId, ObjectIntId)
        </synopsis>
    </refsynopsisdiv>

    <!-- ===================================================================== -->
    <!-- Parameters -->
    <!-- ===================================================================== -->

    <refsection>
        <title>Parameters</title>
        <variablelist>

            <varlistentry>
                <term>fuzzySQL</term>
                <listitem>
                    <para>
                      FuzzySQL pointer object, returned by either DbFuzzyLoadFMB() or DbFuzzyCreateFMB() functions
                    </para>
                </listitem>
            </varlistentry>

            <varlistentry>
                <term>objectIntId</term>
                <listitem>
                    <para>
                      Each object is retrieved from collection by it's identifier.
                      These identifiers may be either integer id's or objects names
                      (not for all objects).
                      For exapmple, FuzzyTableInfo object can be returned by table
                      id or by table name, FuzzyCol object can be retrieved by column id
                      or by full column name
                      (including table name).
                      Some object require 2 identifiers: FuzzyNearnessDef - needs 2 column
                      ids, FuzzyTableQuantifier - needs table id and quantifier name.
                    </para>
                </listitem>
            </varlistentry>

            <varlistentry>
                <term>ObjectStringId</term>
                <listitem>
                  <para>
                    A strign fuzzy object identifer.
                  </para>
                </listitem>
            </varlistentry>

          <varlistentry>
            <term>fuzzyObjectStruct</term>
            <listitem>
              <para>
                a struct, representing a fuzzy object with all same fields, which are stored in the database
              </para>
            </listitem>
          </varlistentry>


        </variablelist>
    </refsection>


    <!-- ===================================================================== -->
    <!-- Description -->
    <!-- ===================================================================== -->
    <refsection>
        <title>Description</title>
        <para>
        <literal>
        DbFuzzyDelete(...)
        </literal>
              Deletes a fuzzy object from a database by its id(s).
        </para>
    </refsection>


    <!-- ===================================================================== -->
    <!-- Examples -->
    <!-- ===================================================================== -->

    <refsection>
        <title>Examples</title>       
        <programlisting role="example"><![CDATA[
// Opens a “PostgreSQL” connection with a structure
connexionStruct          = struct();
connexionStruct.provider = "postgresql";
connexionStruct.database = "myDataBase";
connexionStruct.hostname = "127.0.0.1" ;
connexionStruct.use      = "myUsername";
connexionStruct.password = "myPassword";

DbConnect(connexionStruct);

//creating FuzzySQL object on default connection;
fuzzySQL = DbFuzzyLoadFMB();

//Deleting a fuzzy column object
fuzCol = DbFuzzyDelete(fuzzySQL, 'FCL', 2);
    ]]></programlisting>
    </refsection>


    <!-- ===================================================================== -->
    <!-- See also -->
    <!-- ===================================================================== -->

    <refsection>
        <title>See Also</title>
        <simplelist type="inline">
        <member>
            <link linkend="DbFuzzyLoadFMB">
                DbFuzzyLoadFMB
            </link>
        </member>
        <member>
            <link linkend="DbFuzzyCreateFMB">
              DbFuzzyCreateFMB
            </link>
        </member>
        </simplelist>
    </refsection>


    <!-- ===================================================================== -->
    <!-- Authors -->
    <!-- ===================================================================== -->

    <refsection>
        <title>Authors</title>
        <simplelist type="vert">
        <member>Igor GRIDCHYN</member>
        </simplelist>
    </refsection>

</refentry>

