<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Copyright (C) Igor GRIDCHYN
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->

<refentry xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook"
          version="5.0-subset Scilab"
          xml:lang="en"
          xml:id="DbLastDbError">

  <refnamediv>
    <refname>DbLastDbError</refname>
    <refpurpose>get the last error, occured during database interaction</refpurpose>
  </refnamediv>

  <!-- ===================================================================== -->
  <!-- Calling Sequence -->
  <!-- ===================================================================== -->
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      DbLastDbError()
      DbLastDbError(dblink)
    </synopsis>
  </refsynopsisdiv>

  <!-- ===================================================================== -->
  <!-- Parameters -->
  <!-- ===================================================================== -->

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>dblink</term>
        <listitem>
          <para>
            a pointer to database connection object (returned by DbConnect())
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <!-- ===================================================================== -->
  <!-- Description -->
  <!-- ===================================================================== -->

  <refsection>
    <title>Description</title>
    <para>
      <literal>
      DbLastDbError()
      </literal>
      Returns last error, occured at default database.
    </para>
    <para>
      <literal>
      DbLastDbError(dblink)
      </literal>
      Returns last error, occured at database, represented by pointer dblink.
    </para>
  </refsection>

  <!-- ===================================================================== -->
  <!-- Examples -->
  <!-- ===================================================================== -->

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
// Opens a “PostgreSQL” connection with a connexion string
dblink = DbConnect("PSQL", ..
"Server=127.0.0.1;Database=myDataBase;User Id=myUsername;Password=myPassword;");
//if not connected, get the error by following call
DbLastDbError()
  ]]></programlisting>
  </refsection>


  <!-- ===================================================================== -->
  <!-- See also -->
  <!-- ===================================================================== -->

  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="DbConnect">
          DbConnect
        </link>
      </member>
      <member>
        <link linkend="DbDisconnect">
          DbDisconnect
        </link>
      </member>
    </simplelist>
  </refsection>

  <!-- ===================================================================== -->
  <!-- Authors -->
  <!-- ===================================================================== -->

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Igor GRIDCHYN</member>
    </simplelist>
  </refsection>

</refentry>
